from collections import OrderedDict

class NoLimitCache:
    _instance = None

    @staticmethod
    def getInstance():
        if NoLimitCache._instance == None:
            NoLimitCache()
        return NoLimitCache._instance

    def __init__(self):
        if NoLimitCache._instance != None:
            raise Exception("This is a singleton class. Use getInstance() method to get the instance.")
        else:
            NoLimitCache._instance = self
        self.cache = OrderedDict()

    def get(self, key):
        if key in self.cache:
            return self.cache[key]
        else:
            return None

    def put(self, key, value):
        self.cache[key] = value

    def delete(self, key):
        if key in self.cache:
            del self.cache[key]