from cache import NoLimitCache
import csv
from datetime import datetime
from collections import OrderedDict

# Set the maximum number of successes allowed per day and time difference in minutes
# also set cache size
MAX_SUCCESS = 20
MAX_TIME_DIFFERENCE = 180
# Set a cache for getting the flights details faster
cache = NoLimitCache.getInstance()

def initLoad():
    # Open the CSV file and create a new list to store the updated rows
    try:
        with open('flights.csv', newline='') as csvfile:
            reader = csv.reader(csvfile)
            header = next(reader)  # save the first row
            rows = [row for row in reader]
    except:
        return {'message': 'could not open the csv file', 'error': 500}

    # Sort the updated rows by arrival time
    try:
        sorted_rows = sorted(rows, key=lambda x: datetime.strptime(x[1], '%H:%M'))
    except:
        return {'message': 'there was a problem with the data in the csv', 'error': 500}
    try:
        add_success_fail(sorted_rows)
    except:
        return {'message': 'problem calculating the success and fail of the flights', 'error': 500}
    
    # Load all data to cache
    list(map(lambda row: cache.put(row[0], {'flight_id': row[0], 'Arrival': row[1], 'Departure': row[2], 'success': row[3]}), sorted_rows))

    # Add the first row back to the beginning of the sorted rows list
    sorted_rows.insert(0, header)

    # Write the updated rows back to the CSV file
    try:
        write_to_file(sorted_rows)
    except:
        return {'message': 'there was a problem writing the data to the csv file'}

def getFlightById(flight_Id):
    try:
        flight_details = cache.get(flight_Id)
    except:
        return {'message': 'could not access the cache properly', 'error': 500}
    if flight_details == None:
        return {'message': 'Flight not found', 'error': 200}
    return {'message': flight_details, 'error': 200}

def add_all_flights(flight_list):
    for flight in flight_list:
        try:
            flight_id = flight["flight ID"]
            arrival = flight["Arrival"]
        except:
            return {'message': 'problem with the reading of the data of the flight', 'error': 500}

        # Delete the flight from the cache if it exists
        cache.delete(flight_id)
        
        index_to_insert = None
        
        for index, (key, value) in enumerate(cache.cache.items()):
            if value["Arrival"] > arrival:
                index_to_insert = index
                break
        
        if index_to_insert is not None:
            # Create a new OrderedDict and insert the flight at the correct position
            new_cache = OrderedDict(list(cache.cache.items())[:index_to_insert] + [(flight_id, flight)] + list(cache.cache.items())[index_to_insert:])
            cache.cache = new_cache
        else:
            # If the flight has the latest arrival, add it to the end of the cache
            cache.put(flight_id, flight)
    
    result = transform_cache_data(cache.cache)

    try:
        add_success_fail(result)
    except:
        return {'message':'problem adding success fail to the data', 'error': 500}
    list(map(lambda row: cache.put(row[0], {'flight_id': row[0], 'Arrival': row[1], 'Departure': row[2], 'success': row[3]}), result))

    try:
        write_to_file(result)
    except:
        return {'message': 'problem writing to the csv file', 'error': 500}
    
    return {'message': 'flights were added to csv', 'error': 200}

def add_success_fail(sorted_rows):
    successes = 0
    # Loop through each row in the CSV file
    for row in sorted_rows:
        # Extract the relevant data from the row
        arrival_time = datetime.strptime(row[1], '%H:%M')
        departure_time = datetime.strptime(row[2], '%H:%M')

        # Calculate the time difference between arrival and departure
        time_difference = (departure_time - arrival_time).total_seconds() // 60

        # Check if the flight is a success
        if time_difference >= MAX_TIME_DIFFERENCE or successes >= MAX_SUCCESS:
            row[3] = 'fail'
        else:
            row[3] = 'success'
            successes += 1

def transform_cache_data(cache_data):
    result = []
    for flight_id, data in cache_data.items():
        arrival = data['Arrival']
        departure = data['Departure']
        success = data['success']
        result.append([flight_id, arrival, departure, success])
    return result

def write_to_file(data):
    # Write the updated rows back to the CSV file
    with open('flights.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(data)


    
    


