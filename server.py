from flask import Flask, jsonify, request
import flights_service

app = Flask(__name__)

# Initialize the success and fail for each flight
flights_service.initLoad()

@app.route('/flights/<flight_id>', methods=['GET'])
def get_flight(flight_id):
    result = flights_service.getFlightById(flight_id)
    response = jsonify(result['message'])
    response.status_code = result['error']
    return response

@app.route('/flights', methods=['POST'])
def add_flights():
    flights = request.json['flights']
    result = flights_service.add_all_flights(flights)
    response = jsonify(result['message'])
    response.status_code = result['error']
    return response

if __name__ == '__main__':
    app.run()